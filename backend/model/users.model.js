const mongoose = require('mongoose');

const AutoIncrement = require('mongoose-sequence')(mongoose);

let userSchema = mongoose.Schema(
  {
    _id: Number,
    name: String,
  }
  // { _id: false }
);
// userSchema.plugin(AutoIncrement);
userSchema.plugin(AutoIncrement, { id: 'users_id_counter', inc_field: '_id' });
let User = (module.exports = mongoose.model('Users', userSchema));
module.exports.get = (callback, limit) => {
  User.find(callback).limit(limit);
};
