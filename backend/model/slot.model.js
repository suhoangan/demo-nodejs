const mongoose = require('mongoose');

const AutoIncrement = require('mongoose-sequence')(mongoose);

let slotSchema = mongoose.Schema(
  {
    _id: Number,
    userId: Number,
    isSlotUserRoot: Boolean,
    children: [],
  }
  // { _id: false }
);
// slotSchema.plugin(AutoIncrement);
slotSchema.plugin(AutoIncrement, { id: 'slots_id_counter', inc_field: '_id' });
let Slot = (module.exports = mongoose.model('Slots', slotSchema));
module.exports.get = (callback, limit) => {
  Slot.find(callback).limit(limit);
};
