Slot = require('../model/slot.model');

exports.index = (req, res) => {
  Slot.get((err, slot) => {
    if (err) {
      res.json({
        status: 'error',
        message: err,
      });
    }

    res.json({
      status: 'success',
      message: 'Got Slot Successfully!',
      data: slot,
    });
  });
};

//create
exports.add = (req, res) => {
  let slot = new Slot();
  slot.userId = req.body.userId ? req.body.userId : slot.userId;

  slot.isSlotUserRoot = req.body.isSlotUserRoot
    ? req.body.isSlotUserRoot
    : slot.isSlotUserRoot;

  slot.children = req.body.children ? req.body.children : slot.children;

  slot.save((err) => {
    if (err) res.json(err);
    res.json({
      message: 'New Slot Added!',
      data: slot,
    });
  });
};

//view
exports.view = (req, res) => {
  Slot.findbyId(req.params.slot_id, (err, slot) => {
    if (err) res.json(err);
    res.json({
      message: 'Slot Details',
      data: slot,
    });
  });
};
//update
exports.update = (req, res) => {
  Slot.findById(req.params.slot_id, (err, slot) => {
    if (err) res.json(err);
    slot.userId = req.body.userId ? req.body.userId : slot.userId;

    slot.isSlotUserRoot = req.body.isSlotUserRoot
      ? req.body.isSlotUserRoot
      : slot.isSlotUserRoot;

    slot.children = req.body.children ? req.body.children : slot.children;
    //save and check
    slot.save((err) => {
      if (err) res.json(err);
      res.json({
        message: 'Slot Updated Successfully',
        data: slot,
      });
    });
  });
};

//delete

exports.delete = (req, res) => {
  Slot.deleteOne(
    {
      _id: req.params.slot_id,
    },
    (err, contact) => {
      if (err) res.send(err);
      res.json({
        status: ' Success',
        message: 'Slot Deleted',
      });
    }
  );
};
