User = require('../model/users.model');

exports.index = (req, res) => {
  User.get((err, user) => {
    if (err) {
      res.json({
        status: 'error',
        message: err,
      });
    }

    res.json({
      status: 'success',
      message: 'Got User Successfully!',
      data: user,
    });
  });
};

//create
exports.add = (req, res) => {
  let user = new User();
  user.name = req.body.name ? req.body.name : user.name;

  user.save((err) => {
    if (err) res.json(err);
    res.json({
      message: 'New User Added!',
      data: user,
    });
  });
};

//view
exports.view = (req, res) => {
  User.findbyId(req.params.user_id, (err, user) => {
    if (err) res.json(err);
    res.json({
      message: 'User Details',
      data: user,
    });
  });
};
//update
exports.update = (req, res) => {
  User.findById(req.params.user_id, (err, user) => {
    if (err) res.json(err);
    user.name = req.body.name ? req.body.name : user.name;
    //save and check
    user.save((err) => {
      if (err) res.json(err);
      res.json({
        message: 'User Updated Successfully',
        data: user,
      });
    });
  });
};

//delete

exports.delete = (req, res) => {
  User.deleteOne(
    {
      _id: req.params.user_id,
    },
    (err, contact) => {
      if (err) res.send(err);
      res.json({
        status: ' Success',
        message: 'User Deleted',
      });
    }
  );
};
