let express = require('express');
let app = express();
let apiRouters = require('./routers');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.options('*', cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

const dbPath =
  // 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false';
  'mongodb+srv://hollad104:herotonight@treeuser.h5zp5.mongodb.net/treeusers?retryWrites=true&w=majority';

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
};
const mongo = mongoose.connect(dbPath, options);

mongo.then(
  () => {
    console.log('connected');
  },
  (error) => {
    console.log(error, 'error');
  }
);
app.use('/api', apiRouters);

var port = process.env.PORT || 5000;

app.listen(port, function () {
  console.log('Running FirstRest on Port ' + port);
});
