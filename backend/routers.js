let router = require('express').Router();

var userController = require('./Controllers/userController');

router.route('/users').get(userController.index);
router.route('/user').post(userController.add);
router
  .route('/user/:user_id')
  .get(userController.view)
  .patch(userController.update)
  .put(userController.update)
  .delete(userController.delete);

//slot
var slotController = require('./Controllers/slotController');
router.route('/slots').get(slotController.index);
router.route('/slot').post(slotController.add);

router
  .route('/slot/:slot_id')
  .get(slotController.view)
  .patch(slotController.update)
  .put(slotController.update)
  .delete(slotController.delete);

module.exports = router;
