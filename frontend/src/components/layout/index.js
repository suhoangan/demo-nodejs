import React from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';

const Layout = ({ children }) => {
  return (
    <div className="layout-wrapper">
      <div className="main-wrap">
        <input id="slide-sidebar" type="checkbox" role="button" />
        <label className="label-toggle" htmlFor="slide-sidebar">
          <input
            type="checkbox"
            className="openSidebarMenu"
            id="openSidebarMenu"
          />
          <label htmlFor="slide-sidebar" className="sidebarIconToggle">
            <div className="spinner diagonal part-1"></div>
            <div className="spinner horizontal"></div>
            <div className="spinner diagonal part-2"></div>
          </label>
        </label>
        <div className="sidebar">
          <ul className="sidebarMenuInner">
            <Link to="/" target="_self" rel="noreferrer">
              <li>Manager User</li>
            </Link>
            <Link to="/slot" target="_self" rel="noreferrer">
              <li>Slots</li>
            </Link>
          </ul>
        </div>
        <div className="portfolio">{children}</div>
      </div>
    </div>
  );
};

export default Layout;
