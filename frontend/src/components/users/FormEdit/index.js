import React, { useRef } from 'react';
import { updateUser } from '../../../services/users.service';
import './styles.scss';
const FormEdit = ({ data }) => {
  const formRef = useRef();
  const edittValue = async () => {
    const form = formRef.current;
    const name = form['name'].value;
    if (name !== null && name !== '') {
      const user = {
        _id: data._id,
        name: name,
      };
      await updateUser({ user });
    }
  };

  return (
    <div className="formEdit-wrapper">
      <form ref={formRef}>
        <div className="form-group">
          <label>Name:</label>
          <input
            required
            type="text"
            className="form-control"
            placeholder="Name"
            name={'name'}
            defaultValue={data.name ? data.name : ''}
          />
        </div>
        <button
          className="btn btn-success btn-save"
          type="submit"
          onClick={() => edittValue()}
        >
          Save Change
        </button>
      </form>
    </div>
  );
};

export default FormEdit;
