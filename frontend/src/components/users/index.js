import React, { useEffect, useState } from 'react';
import { Button, Card, Modal } from 'react-bootstrap';
import * as Icon from 'react-bootstrap-icons';
import { getAllSlot } from '../../services/slots.service';
import { addUser, deleteUser, getAllUser } from '../../services/users.service';
import TreeOneUser from '../chart/TreeOneUser';
import Layout from '../layout';
import FormEdit from './FormEdit';
import './users.style.scss';

const UserView = () => {
  const [fName, setfName] = useState('');
  const [user, setUser] = useState([]);
  const [selectedUserId, setUserId] = useState();
  const [showLg, setShowLg] = useState(false);
  const [reload, setReload] = useState(false);
  const [show, setShow] = useState(false);
  const [dataEdit, setDataEdit] = useState({});
  const [slots, setSlot] = useState([]);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleDelete = (id) => {
    deleteUser(id);
    if (reload === true) {
      setReload(false);
    } else setReload(true);
  };

  const submitValue = async () => {
    if (fName !== null && fName !== '') await addUser(fName);
    if (reload === true) {
      setReload(false);
    } else setReload(true);
  };

  useEffect(() => {
    const fetchUser = async () => {
      const res = await getAllUser();
      setUser(res.data.data);
    };
    const fetchSlot = async () => {
      const res = await getAllSlot();
      setSlot(res.data.data);
    };
    fetchSlot();
    fetchUser();
  }, [reload]);

  return (
    <Layout>
      <div className="header">Manager User</div>
      <div className="user-wrapper">
        <div className="formAdd-wrapper">
          <Card className="card">
            <form>
              <div className="form-group">
                <label>Name</label>
                <input
                  required
                  type="text"
                  className="form-control"
                  id="nameUser"
                  onChange={(e) => setfName(e.target.value)}
                />
              </div>
              <button
                type="submit"
                className="btn btn-success"
                onClick={() => submitValue()}
              >
                Create
              </button>
            </form>
          </Card>
        </div>

        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">View Tree</th>
              <th scope="col">Tool</th>
            </tr>
          </thead>
          <tbody>
            {user.map((item) => {
              return (
                <tr key={item._id}>
                  <td> {item._id}</td>
                  <td> {item.name}</td>
                  <td>
                    <button
                      onClick={() => {
                        setShowLg(true);
                        setUserId(item._id);
                      }}
                      className="btn-sm mr-4 btn btn-secondary"
                    >
                      <Icon.Bezier color="white" />
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn-sm mr-4 btn btn-info"
                      onClick={() => {
                        handleShow();
                        setDataEdit(item);
                      }}
                    >
                      <Icon.Pen />
                    </button>

                    <button
                      className="btn-sm ml-4 btn btn-danger "
                      onClick={() => handleDelete(item._id)}
                    >
                      <Icon.XLg />
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit user</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormEdit data={dataEdit} />
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Body>
        </Modal>
        <Modal
          show={showLg}
          onHide={() => setShowLg(false)}
          backdrop="static"
          dialogClassName="modal-90w"
          aria-labelledby="Modal view tree"
        >
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <TreeOneUser slot={slots} userID={selectedUserId} />
          </Modal.Body>
        </Modal>
      </div>
    </Layout>
  );
};

export default UserView;
