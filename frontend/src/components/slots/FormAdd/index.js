import { Multiselect } from 'multiselect-react-dropdown';
import React, { useEffect, useRef, useState } from 'react';
import { Card } from 'react-bootstrap';
import { addSlot } from '../../../services/slots.service';
import { getAllUser } from '../../../services/users.service';
import './styles.scss';
const FormAdd = ({ data }) => {
  const [child, setChildren] = useState([]);
  const [user, setUser] = useState([]);
  const [selectedUser, setSelectedUser] = useState();
  const [disableBtn, setDisable] = useState(true);
  const formRef = useRef();

  const submitValue = async () => {
    const form = formRef.current;
    const userId = selectedUser;
    const isSlotUserRoot = form['isSlotUserRoot'].value;
    const children = child;
    const slot = {
      userId: userId,
      isSlotUserRoot: isSlotUserRoot,
      children: children,
    };
    if (slot.userId !== '' && slot.userId !== null) {
      await addSlot({ slot });
    } else {
      return null;
    }
  };
  const onSelectedChild = (selectedList) => {
    const listChildren = [];
    selectedList.map((item) => {
      listChildren.push(item._id);
      return setChildren(listChildren);
    });
  };
  const onSelectedUser = (selectedList) => {
    const id = selectedList[0]._id;
    if (selectedList.includes(undefined) || selectedList === []) {
      setDisable(true);
    } else {
      setDisable(false);
    }
    setSelectedUser(id);
  };
  useEffect(() => {
    const getUser = async () => {
      const res = await getAllUser();
      setUser(res.data.data);
    };
    getUser();
  }, []);
  console.log('selectedUser :>> ', selectedUser);
  return (
    <div className="formAdd-wrapper">
      <Card className="card">
        <form ref={formRef}>
          <div className="form-group">
            <label className="form-check-label">User Id</label>
            <Multiselect
              requied
              id="select-user"
              emptyRecordMsg="Please, chooese User Id!"
              closeIcon="cancel"
              showArrow={true}
              options={user}
              displayValue="_id"
              placeholder="Select User"
              onSelect={onSelectedUser}
              selectionLimit="1"
            />
          </div>
          <div className="form-group">
            <label htmlFor="isSlotUserRoot">Is Slot User Root :</label>
            <select
              name={'isSlotUserRoot'}
              className="form-control"
              id="isSlotUserRoot"
            >
              <option value="false">False</option>
              <option value="true">True</option>
            </select>
          </div>
          <div className="form-group">
            <label>Children</label>
            <Multiselect
              id="select-child"
              closeIcon="cancel"
              options={data}
              displayValue="_id"
              placeholder="Select children"
              onSelect={onSelectedChild}
              selectionLimit="2"
            />
          </div>
          <button
            disabled={disableBtn}
            type="submit"
            className="btn btn-success"
            onClick={() => submitValue()}
          >
            Create
          </button>
        </form>
      </Card>
    </div>
  );
};

export default FormAdd;
