import React, { useEffect, useState } from 'react';
import { Badge, Button, Modal } from 'react-bootstrap';
import * as Icon from 'react-bootstrap-icons';
import { deleteSlot, getAllSlot } from '../../services/slots.service';
import TreeAllUser from '../chart/TreeAllUser';
import Layout from '../layout';
import FormAdd from './FormAdd';
import FormEdit from './FormEdit';
import './slots.style.scss';
const SlotView = () => {
  const [slot, setSlot] = useState([]);
  const [reload, setReload] = useState(false);
  const [show, setShow] = useState(false);
  const [showLg, setShowLg] = useState(false);
  const [dataEdit, setDataEdit] = useState({});

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleDelete = async (id) => {
    await deleteSlot(id);
    if (reload === true) {
      setReload(false);
    } else setReload(true);
  };

  const getBage = (value) => {
    if (value === true) {
      return <Badge variant="success">True</Badge>;
    } else return <Badge variant="warning">False</Badge>;
  };
  const getChildren = (data) => {
    return data.map((item) => {
      return (
        <Badge key={item} pill variant="info">
          {item}
        </Badge>
      );
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      const res = await getAllSlot();
      setSlot(res.data.data);
    };
    fetchData();
  }, [reload]);

  return (
    <Layout>
      <div className="header">Manager Slot</div>
      <div className="slot-manager-wrapper">
        <FormAdd data={slot} />
        <Button  variant="info" onClick={() => setShowLg(true)}>
          Show tree
        </Button>
        <br />
        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">User ID</th>
              <th scope="col">Is Slot User Root</th>
              <th scope="col">Children</th>
              <th scope="col">Tool</th>
            </tr>
          </thead>
          <tbody>
            {slot.map((item) => {
              return (
                <tr key={item._id}>
                  <td>{item._id}</td>
                  <td>{item.userId}</td>
                  <td>{getBage(item.isSlotUserRoot)}</td>
                  <td>{getChildren(item.children)}</td>
                  <td>
                    <button
                      className="btn-sm mr-4 btn btn-info"
                      onClick={() => {
                        handleShow();
                        setDataEdit(item);
                      }}
                    >
                      <Icon.Pen />
                    </button>

                    <button
                      className="btn-sm ml-4 btn btn-danger"
                      onClick={() => handleDelete(item._id)}
                    >
                      <Icon.XLg />
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit slot</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormEdit item={dataEdit} slot={slot} />
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Body>
        </Modal>
      </div>

      <Modal
        show={showLg}
        onHide={() => setShowLg(false)}
        backdrop="static"
        dialogClassName="modal-90w"
        aria-labelledby="Modal view tree"
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <TreeAllUser slot={slot} />
        </Modal.Body>
      </Modal>
    </Layout>
  );
};

export default SlotView;
