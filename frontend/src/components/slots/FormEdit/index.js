import { Multiselect } from 'multiselect-react-dropdown';
import React, { useEffect, useRef, useState } from 'react';
import { updateSlot } from '../../../services/slots.service';
import { getAllUser } from '../../../services/users.service';
import './styles.scss';
const FormEdit = ({ item, slot }) => {
  const [fixedData, setFixedData] = useState([]);
  const [child, setChildren] = useState();
  const [currentUserId, setCurrentUserId] = useState();
  const [selectedUser, setSelectedUser] = useState();
  const [filteredData, setFilteredData] = useState([]);

  const [user, setUser] = useState([]);
  const formRef = useRef();

  const submitValue = async () => {
    const form = formRef.current;
    const userId = selectedUser ? selectedUser : currentUserId[0]._id;
    const isSlotUserRoot = form['isSlotUserRoot'].value;
    const children = child;

    const slot = {
      _id: item._id,
      userId: userId,
      isSlotUserRoot: isSlotUserRoot,
      children: children,
    };
    if (item._id && slot.userId !== null && slot.userId !== '') {
      await updateSlot({ slot });
    }
  };
  const onSelectedChild = (selectedList) => {
    const listChildren = [];
    selectedList.map((item) => {
      listChildren.push(item._id);
      return setChildren(listChildren);
    });
  };
  const onSelectedUser = (selectedList) => {
    setSelectedUser(selectedList[0]._id);
  };
  const onRemoveChildren = (selectedList, removedItem) => {
    console.log('selectedList :>> ', selectedList);
    setChildren(selectedList);
  };

  useEffect(() => {
    setCurrentUserId([{ _id: item.userId }]);
    const data = [];
    item.children.map((e) => {
      return data.push({ _id: e });
    });
    setFixedData(data);
    const slotFilterd = slot.filter((e) => e._id !== item._id);
    setFilteredData(slotFilterd);
    const getUser = async () => {
      const res = await getAllUser();
      setUser(res.data.data);
    };
    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  console.log('currentUserId :>> ', child);
  return (
    <div className="formEdit-wrapper">
      <form ref={formRef}>
        <div className="form-group">
          <label className="form-check-label">User Id</label>
          <Multiselect
            id="select-user"
            closeIcon="cancel"
            showArrow={true}
            options={user}
            selectedValues={currentUserId}
            displayValue="_id"
            placeholder="Select User"
            onSelect={onSelectedUser}
            selectionLimit="1"
          />
        </div>
        <div className="form-group">
          <label htmlFor="isSlotUserRoot">Is Slot User Root :</label>
          <select
            defaultValue={item.isSlotUserRoot ? item.isSlotUserRoot : ''}
            name={'isSlotUserRoot'}
            className="form-control"
            id="isSlotUserRoot"
          >
            <option value="false">False</option>
            <option value="true">True</option>
          </select>
        </div>
        <div className="form-group">
          <label>Children</label>
          <Multiselect
            id="select-child"
            closeIcon="cancel"
            showArrow={true}
            options={filteredData}
            selectedValues={fixedData}
            displayValue="_id"
            placeholder="Select children"
            onSelect={onSelectedChild}
            selectionLimit="2"
            onRemove={onRemoveChildren}
          />
        </div>
        <button
          type="submit"
          className="btn btn-success btn-save"
          onClick={() => submitValue()}
        >
          Save Change
        </button>
      </form>
    </div>
  );
};

export default FormEdit;
