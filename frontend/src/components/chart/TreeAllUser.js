import OrgChart from './ChartTheme';

const TreeAllUser = ({ slot }) => {
  let dataConvert = [];
  if (slot !== [] && slot !== '') {
    dataConvert.push(slot[0]);
    slot.forEach((element) => {
      element['id'] = element._id;
      element['tags'] = ['grey'];

      if (element.isSlotUserRoot) {
        element['tags'] = ['blue'];
      }

      if (element.children.length > 0) {
        let parentId = element._id;
        element.children.forEach((childId) => {
          const needPid = slot.find((e) => e._id === childId);
          if (needPid) {
            needPid['pid'] = parentId;
            dataConvert.push(needPid);
          }
        });
      }
    });
  }

  if (dataConvert.includes(undefined)) dataConvert = [];
  return (
    <div id="orgchart" style={{ height: '100%', width: '100%' }}>
      <OrgChart nodes={dataConvert} />
    </div>
  );
};

export default TreeAllUser;
