/* eslint-disable react-hooks/exhaustive-deps */
import OrgChart from '@balkangraph/orgchart.js';
import React, { createRef, useEffect } from 'react';
import './styles.scss';

const ChartTheme = ({ nodes }) => {
  let divRef = createRef();

  useEffect(() => {
    OrgChart.templates.blue = Object.assign({}, OrgChart.templates.ana);
    OrgChart.templates.blue.size = [150, 100];

    OrgChart.templates.blue.node =
      '<rect x="0" y="0" height="100" width="150" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';

    OrgChart.templates.blue.field_0 =
      '<text style="font-size: 24px;" fill="#ffffff" x="75" y="45" text-anchor="middle">{val}</text>';
    //edit content here
    OrgChart.templates.blue.field_1 =
      '<text style="font-size: 16px;" fill="#ffffff" x="75" y="70" text-anchor="middle">UserId {val}</text>';
    OrgChart.templates.blue.nodeMenuButton =
      '<g style="cursor:pointer;" transform="matrix(1,0,0,1,120,80)" control-node-menu-id="{id}"><rect x="-4" y="-10" fill="#000000" fill-opacity="0" width="22" height="22"></rect><line x1="0" y1="0" x2="0" y2="10" stroke-width="2" stroke="#fff" /><line x1="7" y1="0" x2="7" y2="10" stroke-width="2" stroke="#fff" /><line x1="14" y1="0" x2="14" y2="10" stroke-width="2" stroke="#fff" /></g>';

    //template grey
    OrgChart.templates.grey = Object.assign({}, OrgChart.templates.ana);
    OrgChart.templates.grey.size = [150, 100];

    OrgChart.templates.grey.node =
      '<rect x="0" y="0" height="100" width="150" fill="#757575" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';

    OrgChart.templates.grey.field_0 =
      '<text style="font-size: 24px;" fill="#ffffff" x="75" y="45" text-anchor="middle">{val}</text>';
    OrgChart.templates.grey.field_1 =
      '<text style="font-size: 16px;" fill="#ffffff" x="75" y="70" text-anchor="middle">UserId {val}</text>';
    OrgChart.templates.grey.nodeMenuButton =
      '<g style="cursor:pointer;" transform="matrix(1,0,0,1,120,80)" control-node-menu-id="{id}"><rect x="-4" y="-10" fill="#000000" fill-opacity="0" width="22" height="22"></rect><line x1="0" y1="0" x2="0" y2="10" stroke-width="2" stroke="#fff" /><line x1="7" y1="0" x2="7" y2="10" stroke-width="2" stroke="#fff" /><line x1="14" y1="0" x2="14" y2="10" stroke-width="2" stroke="#fff" /></g>';

    var chart = new OrgChart(divRef.current, {
      // nodes: nodes,
      lazyLoading: true,
      alignL: OrgChart.CENTER,
      toolbar: {
        layout: true,
        zoom: true,
        fit: true,
        expandAll: false,
      },
      nodeBinding: {
        field_0: 'id',
        field_1: 'userId',
      },
      tags: {
        blue: {
          template: 'blue',
        },
        grey: {
          template: 'grey',
        },
      },
    });

    chart.load(nodes);
  }, []);
  return <div className="tree-wrapper" id="tree" ref={divRef}></div>;
};

export default ChartTheme;
