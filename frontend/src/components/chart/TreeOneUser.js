import OrgChart from './ChartTheme';

const TreeOneUser = ({ slot, userID }) => {
  let dataConvert = [];
  let dataFilter = [];
  if (slot !== [] && slot !== '' && userID && userID !== null) {
    dataConvert.push(slot[0]);
    slot.forEach((element) => {
      element['id'] = element._id;
      element['tags'] = ['grey'];

      if (element.isSlotUserRoot) {
        element['tags'] = ['blue'];
      }

      if (element.children.length > 0) {
        let parentId = element._id;
        element.children.forEach((childId) => {
          const needPid = slot.find((e) => e._id === childId);
          if (needPid) {
            needPid['pid'] = parentId;
            dataConvert.push(needPid);
          }
        });
      }
    });
    if (dataConvert.includes(undefined)) dataConvert = [];
    const userRoot = slot.find(
      (item) => item.isSlotUserRoot === true && item.userId === userID
    );
    if (userRoot) {
      dataFilter.push(userRoot);
    }
    const a = dataConvert.indexOf(userRoot);
    const b = dataConvert.slice(a);
    b.forEach((e) => {
      dataFilter.forEach((item) => {
        if (e.pid === item._id) {
          dataFilter.push(e);
        }
      });
    });
  }

  if (dataFilter.includes(undefined)) dataFilter = [];
  return (
    <div id="orgchart" style={{ height: '100%', width: '100%' }}>
      <OrgChart nodes={dataFilter} />
    </div>
  );
};

export default TreeOneUser;
