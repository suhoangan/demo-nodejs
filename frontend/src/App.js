import React from 'react';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import SlotView from './components/slots';
import UserView from './components/users';

const App = () => {
  return (
    <BrowserRouter>
      <div className="main-route-place">
        <Route exact path="/" component={UserView} />
        <Route path="/slot" component={SlotView}></Route>
      </div>
    </BrowserRouter>
  );
};
export default App;
