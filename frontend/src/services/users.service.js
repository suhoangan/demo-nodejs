import { request } from '../utils/request';

export const getAllUser = () =>
  request({
    url: '/users',
    method: 'GET',
    params: {},
    data: {},
  });

export const addUser = (fname) =>
  request({
    url: '/user',
    method: 'POST',
    params: {},
    data: { name: fname },
  });
export const deleteUser = (id) =>
  request({
    url: `/user/${id}`,
    method: 'DELETE',
    params: {},
    data: {},
  });
export const updateUser = ({ user }) =>
  request({
    url: `/user/${user._id}`,
    method: 'PUT',
    params: {},
    data: { _id: user._id, name: user.name },
  });
