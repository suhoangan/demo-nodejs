import { request } from '../utils/request';

export const getAllSlot = async () =>
  await request({
    url: '/slots',
    method: 'GET',
    params: {},
    data: {},
  });

export const addSlot = async ({ slot }) =>
  await request({
    url: '/slot',
    method: 'POST',
    params: {},
    data: {
      userId: slot.userId,
      isSlotUserRoot: slot.isSlotUserRoot,
      children: slot.children,
    },
  });
export const deleteSlot = async (id) =>
  await request({
    url: `/slot/${id}`,
    method: 'DELETE',
    params: {},
    data: {},
  });
export const updateSlot = async ({ slot }) =>
  await request({
    url: `/slot/${slot._id}`,
    method: 'PUT',
    params: {},
    data: {
      _id: slot._id,
      userId: slot.userId,
      isSlotUserRoot: slot.isSlotUserRoot,
      children: slot.children,
    },
  });
